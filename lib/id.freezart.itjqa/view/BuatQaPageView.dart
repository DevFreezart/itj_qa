import 'package:flutter/material.dart';
import 'package:itjqa/id.freezart.itjqa/viewmodel/BuatQaPage.dart';

class BuatQaPageView extends BuatQaPageState {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        children: <Widget>[
          TextField(
            controller: txtCategory,
            decoration: InputDecoration(
              hintText: "Category",
              labelText: "Category",
            ),
          ),
          TextField(
            controller: txtDesc,
            decoration: InputDecoration(
              hintText: "Description",
              labelText: "Description",
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: ButtonTheme(
              minWidth: double.infinity,
              child: MaterialButton(
                color: Colors.blueAccent,
                onPressed: () => addCategory(),
                child: Text('Tambah'),
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: buatList.length,
              itemBuilder: (_, index) => Card(
                  elevation: 1.0,
                  margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  child: ListTile(
                    leading: InkWell(
                      child: CircleAvatar(
                        backgroundColor: Colors.blue,
                        foregroundColor: Colors.white,
                        child: Text((index+1).toString()),
                      ),
                      onTap: () => clearText(),
                    ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(buatList[index].name,
                            maxLines: 1, overflow: TextOverflow.ellipsis),
                        Text(buatList[index].desc,
                            maxLines: 5,
                            style: TextStyle(color: Colors.grey),
                            overflow: TextOverflow.ellipsis),
                      ],
                    ),
                    trailing: new InkWell(
                      child: Icon(Icons.delete),
                    ),
                  )
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
