import 'package:flutter/material.dart';
import './ContainerMain.dart';

class Home extends StatefulWidget {
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String anu = "home";

  void _buatPertanyaan() {
    setState(() {
      anu = "buat";
    });
  }

  void _jawabPertanyaan() {
    setState(() {
      anu = "jawab";
    });
  }

  void _signOut() {
    setState(() {
      anu = "logout";
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: new Text("#tanpaJIL"),
        ),
        drawer: new Drawer(
          child: new ListView(
            children: <Widget>[
              new UserAccountsDrawerHeader(
                accountName: new Text(""),
                accountEmail: new Text(""),
                decoration: new BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("img/tanpajil.jpeg"),
                      fit: BoxFit.cover),
                ),
              ),
              new GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                  _buatPertanyaan();
                },
                child: new ListTile(
                  title: new Text("Buat Pertanyaan"),
                  trailing: new Icon(Icons.event_note),
                ),
              ),
              new GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                  _jawabPertanyaan();
                },
                child: new ListTile(
                  title: new Text("Jawab Pertanyaan"),
                  trailing: new Icon(Icons.event_available),
                ),
              ),
              new GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                  _signOut();
                },
                child: new ListTile(
                  title: new Text("Signout"),
                  trailing: new Icon(Icons.power_settings_new),
                ),
              )
            ],
          ),
        ),
        body: ContainerMain(judul:anu), 
    );
  }
}