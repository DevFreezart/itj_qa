import 'package:flutter/material.dart';
import 'package:itjqa/id.freezart.itjqa/model/Category.dart';
import 'package:itjqa/id.freezart.itjqa/view/BuatQaPageView.dart';

class BuatQaPage extends StatefulWidget {
  @override
  BuatQaPageView createState() => new BuatQaPageView();
}

abstract class BuatQaPageState extends State<BuatQaPage> {
  @protected
  List<Category> buatList = new List();

  @protected
  TextEditingController txtCategory = new TextEditingController();

  @protected
  TextEditingController txtDesc = new TextEditingController();

  @protected
  void addCategory() {
    var category = new Category();
    category.name = txtCategory.text;
    category.desc = txtDesc.text;
    setState(() => buatList.add(category));
    clearText();
  }

  @protected
  void clearText() {
    txtCategory.clear();
    txtDesc.clear();
  }
}
