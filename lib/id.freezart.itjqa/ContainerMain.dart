import 'package:flutter/material.dart';
import 'package:itjqa/id.freezart.itjqa/viewmodel/BuatQaPage.dart';
import 'package:itjqa/id.freezart.itjqa/viewmodel/BuatSoalPage.dart';

class ContainerMain extends StatelessWidget {

  ContainerMain({this.judul});

  final String judul;

  
  @override
  Widget build(BuildContext context) {
    if(judul == "home"){
      return new Container(
        child: new Text("Home"),
      );
    }
    if(judul == "buat"){
      return new Container(
        margin: EdgeInsets.all(10.0),
        child: new BuatQaPage(),
      );
    }
    if(judul == "jawab"){
      return new Container(
        margin: EdgeInsets.all(5.0),
        child: new BuatSoalPage(),
      );
    }
    if(judul == "logout"){
      return new Container(
        child: new Text("Logout Coy"),
      );
    }
    return null;
  }
}